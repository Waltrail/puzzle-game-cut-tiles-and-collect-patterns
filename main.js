var tiles = [];
var offset;
var cuttingPoints = [];
var CuttingState = false;
var cutStart = false;
var middleCut = false;
var zoom = 0.5;
var cutEnd = false;
var ChangeStateButton;
var FormStateButton;
var ShowExpButton;
var cuttingElement = Infinity;
var wall;
var exp;
var grid;
var skipTouch = 0;
var showIMG = false;
var AspectX;
var AspectY;

function preload() {
	wall = loadImage('Images/wall.jpg');
	exp = loadImage('Images/exp.jpg');
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	frameRate(60);
	AspectX = 2133 / width;
	AspectY = 1076 / height;
	camera.zoom = zoom;
	camera.on();
	camera.position = createVector(width, height);
	textSize(width);
	textAlign(CENTER, CENTER);
	for (let i = 0; i < 20; i++) {
		tiles.push(
			new tile(color(240, 30, 40), new Vector2(20, 20), [
				new Vector2(0, 200),
				new Vector2(0, 0),
				new Vector2(200, 0),
				new Vector2(200, 200)
			])
		);
		tiles.push(
			new tile(color(120, 170, 225), new Vector2(20, 220), [
				new Vector2(0, 200),
				new Vector2(0, 0),
				new Vector2(200, 0),
				new Vector2(200, 200)
			])
		);
	}
	for (let i = 0; i < 2; i++) {
		tiles.push(
			new tile(color(20, 185, 70), new Vector2(20, 420), [
				new Vector2(0, 200),
				new Vector2(0, 0),
				new Vector2(200, 0),
				new Vector2(200, 200)
			])
		);
	}
	ChangeStateButton = new Button(
		'set cutting',
		new Vector2(width - 600 / AspectX, height + 750 / AspectY),
		new Vector2(400, 200),
		color(30, 30, 90)
	);
	FormStateButton = new Button(
		'grid edit',
		new Vector2(width + 200 / AspectX, height + 750 / AspectY),
		new Vector2(400, 200),
		color(30, 30, 90)
	);
	ShowExpButton = new Button('пример', new Vector2(width + 1850 / AspectX, 100), new Vector2(200, 100), color(30, 30, 90));
	grid = new Grid(new Vector2(1040 / AspectX, 60 / AspectY));
}

function draw() {
	background(255);
	image(wall, 0, 0, (3300 + 990) / AspectX, (2200 + 660) / AspectY);
	colorMode(RGB, 100);
	strokeWeight(4);

	tiles.forEach((tile) => {
		tile.draw();
	});
	for (let i = 0; i < tiles.length; i++) {
		if (tiles[i].dragging) {
			tiles[i].position = new Vector2(mouseX / zoom + offset.x, mouseY / zoom + offset.y);
			break;
		}
	}
	for (let i = 0; i < cuttingPoints.length; i++) {
		if (cuttingElement != Infinity) {
			fill(255);
			circle(
				cuttingPoints[i].x + tiles[cuttingElement].position.x,
				cuttingPoints[i].y + tiles[cuttingElement].position.y,
				10
			);
		}
	}
	if (cutEnd) {
		if (cuttingPoints.length == 3) {
			if (is_point_on_segment(cuttingPoints[0], cuttingPoints[1], cuttingPoints[2])) {
				cuttingPoints.splice(1, 1)
			}
		}
		tiles[cuttingElement].reconstract(cuttingPoints);
		cuttingPoints = [];
		cutEnd = false;
		cutStart = false;
		middleCut = false;
	}
	if (CuttingState) {
		ChangeStateButton.text = 'set dragging';
	} else {
		ChangeStateButton.text = 'set cutting';
	}
	grid.draw();
	if (showIMG) {
		image(exp, 2750 / AspectX, 0, 1143 / AspectX, 1365 / AspectY);
	}
	ChangeStateButton.draw();
	FormStateButton.draw();
	ShowExpButton.draw(40);
	strokeWeight(2);
	fill(20);
	textAlign(LEFT, CENTER);
	textSize(50);
	text('Выполнить:', 10, 870);
	textSize(44);
	text('разметку основания', 30, 950);
	text('установку опорной рейки', 30, 1030);
	text('разметку и резку плитки согласно чертежу', 30, 1110);
	if (skipTouch != 0)
		skipTouch--;
}

function mousePressed() {
	if (skipTouch != 0)
		return;
	skipTouch = 20;
	if (
		mouseX / zoom > ChangeStateButton.position.x &&
		mouseX / zoom < ChangeStateButton.position.x + ChangeStateButton.size.x &&
		mouseY / zoom > ChangeStateButton.position.y &&
		mouseY / zoom < ChangeStateButton.position.y + ChangeStateButton.size.y
	) {
		if (CuttingState) {
			CuttingState = false;
			ChangeStateButton.text = 'set cutting';
		} else {
			CuttingState = true;
			ChangeStateButton.text = 'set dragging';
		}
	}
	if (
		mouseX / zoom > FormStateButton.position.x &&
		mouseX / zoom < FormStateButton.position.x + FormStateButton.size.x &&
		mouseY / zoom > FormStateButton.position.y &&
		mouseY / zoom < FormStateButton.position.y + FormStateButton.size.y
	) {
		var Rwidth = parseInt(document.getElementById('input').value);
		var Rheight = parseInt(document.getElementById('input1').value);
		var thickness = parseInt(document.getElementById('input2').value);
		var rows = parseInt(document.getElementById('input3').value);
		var cols = parseInt(document.getElementById('input4').value);
		grid.init(new Vector2(Rwidth, Rheight), thickness, rows, cols);
	}
	if (
		mouseX / zoom > ShowExpButton.position.x &&
		mouseX / zoom < ShowExpButton.position.x + ShowExpButton.size.x &&
		mouseY / zoom > ShowExpButton.position.y &&
		mouseY / zoom < ShowExpButton.position.y + ShowExpButton.size.y
	) {
		if (showIMG) showIMG = false;
		else showIMG = true;
	}

	if (CuttingState) {
		for (let i = 0; i < tiles.length; i++) {
			if (tiles[i].isPointInside(new Vector2(mouseX / zoom, mouseY / zoom))) {
				if (cutStart && !middleCut) {
					let center = GetCenterPoint(tiles[i].vertexes);
					if (
						Math.sqrt(
							Math.pow(mouseX / zoom - center.x - tiles[i].position.x, 2) +
							Math.pow(mouseY / zoom - center.y - tiles[i].position.y, 2),
							2
						) <= 40
					) {
						if (!isPointExist(center, cuttingPoints)) {
							center.state = 1;
							cuttingPoints.push(center);
							middleCut = true;
						}
						return;
					}
				}

				for (let j = 0; j < tiles[i].vertexes.length; j++) {
					if (
						GetDistanceBetweenPoints(
							tiles[i].vertexes[j],
							new Vector2(mouseX / zoom - tiles[i].position.x, mouseY / zoom - tiles[i].position.y)
						) < 25
					) {
						if (!isPointExist(tiles[i].vertexes[j], cuttingPoints)) {
							let newCut = Object.assign({}, tiles[i].vertexes[j]);
							newCut.state = 1;
							cuttingPoints.push(newCut);
							if (!cutStart) {
								cuttingElement = i;
								cutStart = true;
							} else {
								cutEnd = true;
								CuttingState = false;
							}
						}
						return;
					}
				}
				for (let j = 0; j < tiles[i].vertexes.length; j++) {
					if (j == 0) {
						if (
							GetDistanceFromLine(
								tiles[i].vertexes[0],
								tiles[i].vertexes[tiles[i].vertexes.length - 1],
								new Vector2(mouseX / zoom - tiles[i].position.x, mouseY / zoom - tiles[i].position.y)
							) < 26
						) {
							let pointOnLine = GetClosestPointOnLineSegment(
								tiles[i].vertexes[0],
								tiles[i].vertexes[tiles[i].vertexes.length - 1],
								new Vector2(mouseX / zoom - tiles[i].position.x, mouseY / zoom - tiles[i].position.y)
							);
							let percent = GetPercentFromPointOnLine(
								tiles[i].vertexes[0],
								tiles[i].vertexes[tiles[i].vertexes.length - 1],
								pointOnLine
							);
							if (percent < 60 && percent > 40) {
								cutPoint = GetPointByPercentOnLine(
									tiles[i].vertexes[0],
									tiles[i].vertexes[tiles[i].vertexes.length - 1],
									50
								);
							} else if (percent >= 60) {
								cutPoint = GetPointByPercentOnLine(
									tiles[i].vertexes[0],
									tiles[i].vertexes[tiles[i].vertexes.length - 1],
									100 - percent
								);
							} else if (percent <= 40) {
								cutPoint = GetPointByPercentOnLine(
									tiles[i].vertexes[0],
									tiles[i].vertexes[tiles[i].vertexes.length - 1],
									100 - percent
								);
							}
							if (!isPointExist(tiles[i].vertexes[j], cutPoint)) {
								cutPoint.state = 1;
								cuttingPoints.push(cutPoint);
								if (!cutStart) {
									cuttingElement = i;
									cutStart = true;
								} else {
									cutEnd = true;
									CuttingState = false;
								}
							}
							return;
						}
					} else if (
						j > 0 &&
						GetDistanceFromLine(
							tiles[i].vertexes[j - 1],
							tiles[i].vertexes[j],
							new Vector2(mouseX / zoom - tiles[i].position.x, mouseY / zoom - tiles[i].position.y)
						) < 26
					) {
						let pointOnLine = GetClosestPointOnLineSegment(
							tiles[i].vertexes[j - 1],
							tiles[i].vertexes[j],
							new Vector2(mouseX / zoom - tiles[i].position.x, mouseY / zoom - tiles[i].position.y)
						);
						let percent = GetPercentFromPointOnLine(
							tiles[i].vertexes[j - 1],
							tiles[i].vertexes[j],
							pointOnLine
						);
						if (percent < 60 && percent > 40) {
							cutPoint = GetPointByPercentOnLine(tiles[i].vertexes[j - 1], tiles[i].vertexes[j], 50);
						} else if (percent >= 60) {
							cutPoint = GetPointByPercentOnLine(
								tiles[i].vertexes[j - 1],
								tiles[i].vertexes[j],
								100 - percent
							);
						} else if (percent <= 40) {
							cutPoint = GetPointByPercentOnLine(
								tiles[i].vertexes[j - 1],
								tiles[i].vertexes[j],
								100 - percent
							);
						}
						if (!isPointExist(tiles[i].vertexes[j], cutPoint)) {
							cutPoint.state = 1;
							cuttingPoints.push(cutPoint);
							if (!cutStart) {
								cuttingElement = i;
								cutStart = true;
							} else {
								cutEnd = true;
								CuttingState = false;
							}
						}
						return;
					}
				}
			}
		}
		return;
	}
	for (let i = 0; i < tiles.length; i++) {
		if (tiles[i].isPointInside(new Vector2(mouseX / zoom, mouseY / zoom))) {
			tiles[i].dragging = true;
			offset = new Vector2(tiles[i].position.x - mouseX / zoom, tiles[i].position.y - mouseY / zoom);
			break;
		}
	}
}

function mouseReleased() {
	for (let i = 0; i < tiles.length; i++) {
		tiles[i].dragging = false;
	}
}

class tile {
	constructor(color, position, vertexes = []) {
		this.vertexes = vertexes;
		this.position = position;
		this.color = color;
		this.dragging = false;
	}
	isPointInside(point) {
		let q_patt = [
			[0, 1],
			[3, 2]
		];
		if (this.vertexes < 3) return false;
		let pred_pt = Object.assign({}, this.vertexes.slice(-1)[0]);
		pred_pt.x += this.position.x;
		pred_pt.y += this.position.y;
		pred_pt.x -= point.x;
		pred_pt.y -= point.y;
		let y, x;
		if (pred_pt.y < 0) {
			y = 1;
		} else {
			y = 0;
		}
		if (pred_pt.x < 0) {
			x = 1;
		} else {
			x = 0;
		}
		let pred_q = q_patt[y][x];
		let w = 0;
		for (let i = 0; i < this.vertexes.length; i++) {
			let cur_pt = Object.assign({}, this.vertexes[i]);
			cur_pt.x += this.position.x;
			cur_pt.y += this.position.y;
			cur_pt.x -= point.x;
			cur_pt.y -= point.y;
			if (cur_pt.y < 0) {
				y = 1;
			} else {
				y = 0;
			}
			if (cur_pt.x < 0) {
				x = 1;
			} else {
				x = 0;
			}
			let q = q_patt[y][x];

			switch (q - pred_q) {
				case -3:
					++w;
					break;
				case 3:
					--w;
					break;
				case -2:
					if (pred_pt.x * cur_pt.y >= pred_pt.y * cur_pt.x) ++w;
					break;
				case 2:
					if (!(pred_pt.x * cur_pt.y >= pred_pt.y * cur_pt.x)) --w;
					break;
			}
			pred_pt = cur_pt;
			pred_q = q;
		}
		return w != 0;
	}
	reconstract(cuts) {
		if (this.vertexes.length > 6 || cuts.length > 3) {
			return;
		}
		if (cuts.length < 3) {
			for (let j = 1; j < cuts.length; j++) {
				for (let i = 0; i < this.vertexes.length; i++) {
					if (i == 0) {
						if (
							is_point_on_segment(this.vertexes[0], cuts[j], this.vertexes[this.vertexes.length - 1]) &&
							is_point_on_segment(this.vertexes[0], cuts[j - 1], this.vertexes[this.vertexes.length - 1])
						) {
							return;
						}
					} else {
						if (
							is_point_on_segment(this.vertexes[i - 1], cuts[j], this.vertexes[i]) &&
							is_point_on_segment(this.vertexes[i - 1], cuts[j - 1], this.vertexes[i])
						) {
							return;
						}
					}
				}
			}
		}
		let center = GetCenterPoint(this.vertexes);
		for (let i = 0; i < tiles.length; i++) {
			if (tiles[i] === this) {
				tiles.splice(i, 1);
				break;
			}
		}
		let points = [];
		points.push(...this.vertexes);
		let sortedCuts;
		let start = 1;
		if (isPointExist(cuts[0], points) || isPointExist(cuts[cuts.length - 1], points)) {
			start = 2;
		}
		let uwu = false;
		if (cuts[0].x == 0 && cuts[0].y == 0 ||
			cuts[cuts.length - 1].x == 0 && cuts[cuts.length - 1].y == 0) {
			if (is_point_on_segment(points[0], cuts[0], points[1]) && cuts[0].y != 0 ||
				is_point_on_segment(points[0], cuts[cuts.length - 1], points[1]) && cuts[cuts.length - 1].y != 0) {
				uwu = true;
			}
			start = 1;
		}
		loop: for (let i = start; i < points.length + start - 1; i++) {
			for (let j = 0; j < cuts.length; j++) {
				if (i >= points.length && is_point_on_segment(points[0], cuts[j], points[points.length - 1])) {
					if (j == cuts.length - 1) {
						sortedCuts = cuts.reverse();
					} else {
						sortedCuts = cuts;
					}
					for (let y = 0; y < sortedCuts.length; y++) {
						points.insert(i + y, Object.assign({}, sortedCuts[y]));
					}
					let p = [];
					for (let l = 0; l < points.length; l++) {
						p.push(points[l]);
					}
					if (points.length > 10) {
						return;
					}
					for (let k = i + cuts.length; k < points.length; k++) {
						if (
							is_point_on_segment(
								points[k - 1 - cuts.length],
								sortedCuts[sortedCuts.length - 1],
								points[k]
							)
						) {
							points.splice(k, 1);
							break loop;
						} else if (
							k < points.length - 1 &&
							is_point_on_segment(points[k + 1], sortedCuts[sortedCuts.length - 1], points[k]) &&
							points[k + 1].state != 1
						) {
							points.splice(k, 1);
							break loop;
						}
						points.splice(k, 1);
						k--;
					}
				} else if (i < points.length && is_point_on_segment(points[i - 1], cuts[j], points[i])) {
					if (j == cuts.length - 1) {
						sortedCuts = cuts.reverse();
					} else {
						sortedCuts = cuts;
					}
					if (cuts[cuts.length - 1].x == points[i - 1].x && cuts[cuts.length - 1].y == points[i - 1].y) {
						sortedCuts.reverse();
					}
					if (uwu) {
						sortedCuts.reverse();
					}
					for (let y = 0; y < sortedCuts.length; y++) {
						points.insert(i + y, Object.assign({}, sortedCuts[y]));
					}
					if (points.length > 10) {
						return;
					}
					for (let k = i + cuts.length; k < points.length; k++) {
						if (is_point_on_segment(
								points[k - 1 - cuts.length],
								sortedCuts[sortedCuts.length - 1],
								points[k]
							) && sortedCuts[0].x == points[k - 1 - cuts.length].x &&
							sortedCuts[0].y == points[k - 1 - cuts.length].y) {
							break loop;
						} else if (
							is_point_on_segment(
								points[k - 1 - cuts.length],
								sortedCuts[sortedCuts.length - 1],
								points[k]
							)
						) {
							points.splice(k, 1);
							break loop;
						} else if (
							k < points.length - 1 &&
							is_point_on_segment(points[k + 1], sortedCuts[sortedCuts.length - 1], points[k]) &&
							points[k + 1].state != 1
						) {
							points.splice(k, 1);
							break loop;
						}
						points.splice(k, 1);
						k--;
					}
				}
			}
			if (i == points.length - 1 && start == 1) {
				tiles.push(this);
				return;
			}
		}
		let newobjPoints = [];
		newobjPoints = this.vertexes.filter((el) => !points.includes(el));
		for (let y = 0; y < sortedCuts.length; y++) {
			newobjPoints.insert(y, Object.assign({}, sortedCuts[y]));
		}
		SortPointsClockwise(newobjPoints);
		let minX = Infinity,
			minY = Infinity;
		for (let i = 0; i < points.length; i++) {
			for (let j = i + 1; j < points.length; j++) {
				if (points[i].x == points[j].x && points[i].y == points[j].y) {
					if (points[i].state == 1) {
						points.splice(j, 1);
					} else {
						points.splice(i, 1);
						i--;
						break;
					}
				}
			}
		}

		for (let i = 0; i < points.length; i++) {
			if (points[i].x < minX) {
				minX = points[i].x;
			}
			if (points[i].y < minY) {
				minY = points[i].y;
			}
		}
		for (let i = 0; i < points.length; i++) {
			points[i].x -= minX;
			points[i].y -= minY;
		}
		if (cuttingPoints.length > 2 && points.length > 4) {
			let indexCenter = -1;
			for (let i = 0; i < point.length; i++) {
				if (points[i].state == 1 &&
					points[i].x == center.x &&
					points[i].y == center.y) {
					indexCenter = i;
					break;
				}
			}
			if (indexCenter >= 0) {
				let index;
				for (let i = 0; i < point.length; i++) {
					if (points[i].state == 1 &&
						!(points[i].x == center.x &&
							points[i].y == center.y)) {
						index = i;
						break;
					}
				}
				points = moveElement(points, indexCenter, index);
			}
		}
		for (let i = 0; i < points.length; i++) {
			points[i].state = 0;
		}
		if (points.length < 5) {
			for (let i = 0; i < points.length; i++) {
				if (points[i].x == 0 && points[i].y == 0) {
					points = moveElement(points, i, 0);
				}
			}
			SortPointsClockwise(points);
		}
		tiles.push(new tile(this.color, new Vector2(this.position.x + minX, this.position.y + minY), points));
		minX = Infinity;
		minY = Infinity;
		for (let i = 0; i < newobjPoints.length; i++) {
			for (let j = i + 1; j < newobjPoints.length; j++) {
				if (newobjPoints[i].x == newobjPoints[j].x && newobjPoints[i].y == newobjPoints[j].y) {
					if (newobjPoints[i].state == 1) {
						newobjPoints.splice(j, 1);
					} else {
						newobjPoints.splice(i, 1);
						i--;
						break;
					}
				}
			}
		}
		for (let i = 0; i < newobjPoints.length; i++) {
			if (newobjPoints[i].x < minX) {
				minX = newobjPoints[i].x;
			}
			if (newobjPoints[i].y < minY) {
				minY = newobjPoints[i].y;
			}
		}
		for (let i = 0; i < newobjPoints.length; i++) {
			newobjPoints[i].x -= minX;
			newobjPoints[i].y -= minY;
		}
		if (cuttingPoints.length > 2 && newobjPoints.length > 4) {
			let indexCenter = -1;
			for (let i = 0; i < newobjPoints.length; i++) {
				if (newobjPoints[i].state == 1 &&
					newobjPoints[i].x == center.x &&
					newobjPoints[i].y == center.y) {
					indexCenter = i;
					break;
				}
			}
			if (indexCenter >= 0) {
				let index;
				for (let i = 0; i < newobjPoints.length; i++) {
					if (newobjPoints[i].state == 1 &&
						!(newobjPoints[i].x == center.x &&
							newobjPoints[i].y == center.y)) {
						index = i;
						break;
					}
				}
				newobjPoints = moveElement(newobjPoints, indexCenter, index);
			}
		}
		console.log(newobjPoints.slice(0));
		for (let i = 0; i < newobjPoints.length; i++) {
			newobjPoints[i].state = 0;
		}
		if (newobjPoints.length < 5) {
			for (let i = 0; i < newobjPoints.length; i++) {
				if (newobjPoints[i].x == 0 && newobjPoints[i].y == 0) {
					newobjPoints = moveElement(newobjPoints, i, 0);
				}
			}
			SortPointsClockwise(newobjPoints);
		}
		tiles.push(new tile(this.color, new Vector2(this.position.x + minX, this.position.y + minY), newobjPoints));
	}
	draw() {
		fill(this.color);
		//noFill();
		beginShape();
		this.vertexes.forEach((vert) => {
			vertex(vert.x + this.position.x, vert.y + this.position.y);
		});
		endShape(CLOSE);
	}
}
class Vector2 {
	constructor(x = 0, y = 0, state = 0) {
		this.x = x;
		this.y = y;
		this.state = state; // 0 - origin, 1 - cutten vector
	}
}
class Button {
	constructor(text, position, size, color, action, isVisible = true) {
		this.text = text;
		this.action = action;
		this.position = position;
		this.size = size;
		this.color = color;
		this.isVisible = isVisible;
	}
	draw(text_size = 64) {
		fill(this.color);
		rect(this.position.x, this.position.y, this.size.x, this.size.y, 20);
		textSize(text_size);
		fill(255);

		textAlign(CENTER, CENTER);
		text(this.text, this.position.x + this.size.x / 2, this.position.y + this.size.y / 2);
	}
}
class Grid {
	constructor(position) {
		this.size = new Vector2();
		this.thickness = 2;
		this.position = position;
		this.rows = 0;
		this.cols = 0;
		this.Rwidth = 0;
		this.Rheight = 0;
	}
	init(size, thickness, rows, cols) {
		this.size = size;
		this.thickness = thickness;
		this.rows = rows;
		this.cols = cols;
		this.Rwidth = size.x / cols;
		this.Rheight = size.y / rows;
		this.position = new Vector2(width * AspectX - (size.x / 2 / AspectX), 60 / AspectY)
	}
	draw() {
		noFill();
		stroke(20);
		strokeWeight(this.thickness);
		for (let i = 0; i < this.cols; i++) {
			for (let j = 0; j < this.rows; j++) {
				rect(
					(i * (this.Rwidth + this.thickness - 2)) + this.position.x,
					(j * (this.Rheight + this.thickness - 2)) + this.position.y,
					this.Rwidth,
					this.Rheight
				);
			}
		}
		noStroke();
	}
}
const is_point_on_segment = (startPoint, checkPoint, endPoint) => {
	return (
		((endPoint.y - startPoint.y) * (checkPoint.x - startPoint.x)).toFixed(0) ===
		((checkPoint.y - startPoint.y) * (endPoint.x - startPoint.x)).toFixed(0) &&
		((startPoint.x >= checkPoint.x && checkPoint.x >= endPoint.x) ||
			(startPoint.x <= checkPoint.x && checkPoint.x <= endPoint.x)) &&
		((startPoint.y >= checkPoint.y && checkPoint.y >= endPoint.y) ||
			(startPoint.y <= checkPoint.y && checkPoint.y <= endPoint.y))
	);
};

Array.prototype.insert = function (index, item) {
	this.splice(index, 0, item);
};

const SortPointsClockwise = (points) => {
	let center = GetCenterPoint(points);
	points.sort((a, b) => {
		if (a.x >= 0 && b.x < 0) return 1;
		else if (a.x == 0 && b.x == 0) return a.y > b.y;
		let det = (b.x - center.x) * (a.y - center.y) - (a.x - center.x) * (b.y - center.y);
		if (det < 0) return 1;
		else if (det > 0) return -1;

		let d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y);
		let d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);
		if (d1 > d2) {
			return 1;
		}
		if (d1 < d2) {
			return -1;
		}
		return 0;
	});
	return points;
};

const GetIsLess = (a, b) => {
	if (a.x >= 0 && b.x < 0) return true;
	else if (a.x == 0 && b.x == 0) return a.y > b.y;

	let det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y);

	if (det < 0) return true;
	else if (det > 0) return false;

	let d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y);
	let d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);
	return d1 > d2;
};

const GetCenterPoint = (points) => {
	let pointsSum = new Vector2(0, 0);
	for (i = 0; i < points.length; i++) {
		pointsSum.x = pointsSum.x + points[i].x;
		pointsSum.y = pointsSum.y + points[i].y;
	}
	return new Vector2(pointsSum.x / points.length, pointsSum.y / points.length);
};
const GetDistanceFromLine = (LineA, LineB, point) => {
	return (
		Math.abs(
			(LineB.y - LineA.y) * point.x - (LineB.x - LineA.x) * point.y + LineB.x * LineA.y - LineB.y * LineA.x
		) / Math.sqrt(Math.pow(LineB.y - LineA.y, 2) + Math.pow(LineB.x - LineA.x, 2))
	);
};
const GetDistanceBetweenPoints = (point1, point2) => {
	return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2), 2);
};

const GetClosestPointOnLineSegment = (A, B, P) => {
	let AP = new Vector2(P.x - A.x, P.y - A.y);
	let AB = new Vector2(B.x - A.x, B.y - A.y);
	let magnitudeAB = AB.x * AB.x + AB.y * AB.y;
	let ABAPproduct = AP.x * AB.x + AP.y * AB.y;
	let distance = ABAPproduct / magnitudeAB;
	if (distance < 0) {
		return A;
	} else if (distance > 1) {
		return B;
	} else {
		return new Vector2(A.x + AB.x * distance, A.y + AB.y * distance);
	}
};
const GetPercentFromPointOnLine = (A, B, P) => {
	lineDist = GetDistanceBetweenPoints(A, B);
	pointDist = GetDistanceBetweenPoints(A, P);
	return pointDist / lineDist * 100;
};
const GetPointByPercentOnLine = (A, B, perc) => {
	return new Vector2((A.x * perc + B.x * (100 - perc)) / 100, (A.y * perc + B.y * (100 - perc)) / 100);
};
const isPointExist = (Point, arr) => {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i].x == Point.x && arr[i].y == Point.y) {
			return true;
		}
	}
	return false;
};
const DeletePointIfExist = (Point, arr) => {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i].x == Point.x && arr[i].y == Point.y) {
			console.log('del');
			console.log(arr.splice(i--, 1));

		}
	}
};
const setInputFilter = (textbox, inputFilter) => {
	['input', 'keydown', 'keyup', 'mousedown', 'mouseup', 'select', 'contextmenu', 'drop'].forEach(function (event) {
		textbox.addEventListener(event, function () {
			if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			} else if (this.hasOwnProperty('oldValue')) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			}
		});
	});
};
const GetNewArrayWithoutFirstElements = (array, offset) => {
	let newArr = array.slice(0);
	for (let i = 0; i < offset; i++) {
		newArr.shift()
	}
	return newArr;
}
const moveElement = (arr, old_index, new_index) => {
	while (old_index < 0) {
		old_index += arr.length;
	}
	while (new_index < 0) {
		new_index += arr.length;
	}
	if (new_index >= arr.length) {
		var k = new_index - arr.length;
		while ((k--) + 1) {
			arr.push(undefined);
		}
	}
	arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
	return arr;
}

setInputFilter(document.getElementById('input'), function (value) {
	return /^\d*$/.test(value);
});
setInputFilter(document.getElementById('input1'), function (value) {
	return /^\d*$/.test(value);
});
setInputFilter(document.getElementById('input2'), function (value) {
	return /^\d*$/.test(value);
});
setInputFilter(document.getElementById('input3'), function (value) {
	return /^\d*$/.test(value);
});
setInputFilter(document.getElementById('input4'), function (value) {
	return /^\d*$/.test(value);
});

function start() {
	document.getElementById('menu').style.display = "none";
}

var bool = false;

function quest() {
	if (!bool) {
		document.getElementById('panel').style.left = "500px";
		document.getElementById('hidden_panel').style.left = "0px";
		bool = true;
		return;
	}
	if (bool) {
		document.getElementById('panel').style.left = "0px";
		document.getElementById('hidden_panel').style.left = "-500px";
		bool = false;
	}
}